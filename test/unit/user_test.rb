require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "password works" do
  	u = User.new(name: "User")
  	u.password = "password"
  	assert u.authenticate?("password"), "Password didn't come out correct!"
  end

  test "mod priv test" do
  	u = User.new(name: "Moddy")
  	u.title = 3
  	assert u.moderatorpriv?, "A moderator wasn't able to access moderator privleges!"
  	u.title = 5
  	assert u.moderatorpriv?, "An admin wasn't able to access moderator privleges!"
  	u.title = 0
  	assert !u.moderatorpriv?, "A user was able to access moderator privleges!"
  end
end


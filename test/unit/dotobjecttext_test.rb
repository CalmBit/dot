require 'test_helper'

class DotobjecttextTest < ActiveSupport::TestCase

  test "view test" do
  	u = User.new(name: "DotUser")
    utwo = User.new(name: "ExternalUser")
  	u.id = 1
  	u.age = 18
    utwo.id = 2
    utwo.age = 18
  	d = Dotobjecttext.new(title: "DotTest", creator: u.id)
  	
  	d.private = true
  	assert d.canView?(u), "User can't view his own private Dot!"
    assert !d.canView?(utwo), "Other user can see private dot!"

  	d.private = false
  	d.nsfw = 1
  	assert d.canView?(u), "Overage user can't view an NSFW dot!"

  	u.age = 16
  	assert !d.canView?(u), "Underage user can view an NSFW dot!"

  	u.title = 5
  	assert d.canView?(u), "Admin couldn't view an NSFW dot!"
  end

end

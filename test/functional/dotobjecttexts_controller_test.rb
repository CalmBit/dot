require 'test_helper'

class DotobjecttextsControllerTest < ActionController::TestCase
  setup do
    @dotobjecttext = dotobjecttexts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dotobjecttexts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dotobjecttext" do
    assert_difference('Dotobjecttext.count') do
      post :create, dotobjecttext: { content: @dotobjecttext.content, created: @dotobjecttext.created, creator: @dotobjecttext.creator, desc: @dotobjecttext.desc, dotdowns: @dotobjecttext.dotdowns, dotups: @dotobjecttext.dotups, nsfw: @dotobjecttext.nsfw, private: @dotobjecttext.private, title: @dotobjecttext.title }
    end

    assert_redirected_to dotobjecttext_path(assigns(:dotobjecttext))
  end

  test "should show dotobjecttext" do
    get :show, id: @dotobjecttext
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dotobjecttext
    assert_response :success
  end

  test "should update dotobjecttext" do
    put :update, id: @dotobjecttext, dotobjecttext: { content: @dotobjecttext.content, created: @dotobjecttext.created, creator: @dotobjecttext.creator, desc: @dotobjecttext.desc, dotdowns: @dotobjecttext.dotdowns, dotups: @dotobjecttext.dotups, nsfw: @dotobjecttext.nsfw, private: @dotobjecttext.private, title: @dotobjecttext.title }
    assert_redirected_to dotobjecttext_path(assigns(:dotobjecttext))
  end

  test "should destroy dotobjecttext" do
    assert_difference('Dotobjecttext.count', -1) do
      delete :destroy, id: @dotobjecttext
    end

    assert_redirected_to dotobjecttexts_path
  end
end

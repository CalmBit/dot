# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140207190933) do

  create_table "comments", :force => true do |t|
    t.text     "content"
    t.integer  "creator"
    t.integer  "asset"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dotobjectimages", :force => true do |t|
    t.string   "title"
    t.text     "desc"
    t.integer  "creator"
    t.integer  "dotdowns"
    t.integer  "dotups"
    t.boolean  "private"
    t.integer  "nsfw"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dotobjecttexts", :force => true do |t|
    t.string   "title"
    t.text     "desc"
    t.text     "content"
    t.datetime "created"
    t.integer  "creator"
    t.integer  "dotdowns"
    t.integer  "dotups"
    t.boolean  "private"
    t.integer  "nsfw"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "follows", :force => true do |t|
    t.integer  "followable_id",                      :null => false
    t.string   "followable_type",                    :null => false
    t.integer  "follower_id",                        :null => false
    t.string   "follower_type",                      :null => false
    t.boolean  "blocked",         :default => false, :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "follows", ["followable_id", "followable_type"], :name => "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], :name => "fk_follows"

  create_table "images", :force => true do |t|
    t.string   "name"
    t.integer  "parent"
    t.integer  "sizeX"
    t.integer  "sizeY"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "images", ["parent"], :name => "index_images_on_parent"

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "password_hash"
    t.string   "salt"
    t.integer  "age"
    t.datetime "joined"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "banned",              :default => false
    t.integer  "title",               :default => 0
    t.string   "reason"
    t.string   "banner"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "bio"
  end

  create_table "votes", :force => true do |t|
    t.boolean  "vote",          :default => false, :null => false
    t.integer  "voteable_id",                      :null => false
    t.string   "voteable_type",                    :null => false
    t.integer  "voter_id"
    t.string   "voter_type"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "votes", ["voteable_id", "voteable_type"], :name => "index_votes_on_voteable_id_and_voteable_type"
  add_index "votes", ["voter_id", "voter_type", "voteable_id", "voteable_type"], :name => "fk_one_vote_per_user_per_entity", :unique => true
  add_index "votes", ["voter_id", "voter_type"], :name => "index_votes_on_voter_id_and_voter_type"

end

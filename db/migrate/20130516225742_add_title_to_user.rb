class AddTitleToUser < ActiveRecord::Migration
  def change
  	add_column :users, :title, :integer, default: 0
  end
end

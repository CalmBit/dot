class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.integer :creator
      t.integer :asset

      t.timestamps
    end
  end
end

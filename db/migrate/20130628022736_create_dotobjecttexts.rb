class CreateDotobjecttexts < ActiveRecord::Migration
  def change
    create_table :dotobjecttexts do |t|
      t.string :title
      t.text :desc
      t.text :content
      t.timestamp :created
      t.integer :creator
      t.integer :dotdowns
      t.integer :dotups
      t.boolean :private
      t.integer :nsfw

      t.timestamps
    end
  end
end

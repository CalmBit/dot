class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name
      t.integer :parent
      t.integer :sizeX
      t.integer :sizeY

      t.timestamps
    end
    add_index :images, :parent
  end
end

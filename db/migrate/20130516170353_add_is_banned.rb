class AddIsBanned < ActiveRecord::Migration
  def up
  	add_column :users, :banned, :boolean, default: false
  end

  def down
  end
end

class CreateDotobjectimages < ActiveRecord::Migration
  def change
    create_table :dotobjectimages do |t|
      t.string :title
      t.text :desc
      t.integer :creator
      t.integer :dotdowns
      t.integer :dotups
      t.boolean :private
      t.integer :nsfw

      t.timestamps
    end
  end
end

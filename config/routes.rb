  Dot::Application.routes.draw do



  resources :dotobjecttexts do
    member do
      post :votefor
      post :voteagainst
    end
  end



  get "sessions/home"
  
  get "sessions/login"

  get "sessions/setting"

  get "users/ban_user"

  get "users/unban_user"

  get "sessions/follow_user"

  resources :users

  #resources :comments

  #resources :sessions

  get "home/index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'sessions#home'
  match "home", :to => "sessions#home"
	match "signup", :to => "users#new"
	match "login", :to => "sessions#login"
	match "logout", :to => "sessions#logout"
	match "/user/profile", :to => "sessions#profile", :as => :profile, :via => [:get]
  match "/dots/text/", :to => "dotobjecttexts#show", :as => :view_text_dot, :via => [:get]
	match "setting", :to => "sessions#setting"
	match "login_attempt", to: "sessions#login_attempt", :as => :login_attempt
  match "/user/follow_user/:id", :to => "sessions#follow_user", :as => :follow_user
  match "/users/ban_user/:id", :to => "users#ban_user"
  match "/users/unban_user/:id", :to => "users#unban_user"
  match "/post/text", :to => "dotobjecttexts#new"
  match "myhome", :to => "sessions#feed"
  match "/users/invalid_user", :to => "sessions#invalid_user"
  match "/comments/create", :to => "comments#create", :as => :comment_create
  match "/directory", :to => "sessions#directory", :as => :directory

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end

class User < ActiveRecord::Base
  require 'digest'
  attr_accessible :age, :joined, :name, :password_hash, :salt, :password, :avatar, :title
  attr_accessor :password

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/:style/missing.png"

  acts_as_voter
  
  has_many :dotobjecttexts

  acts_as_follower
  acts_as_followable

  def password=(value)
	if value.present?
	  self.salt = (0...10).map{(65+rand(26)).chr}.join
	  self.password_hash = Digest::SHA256.hexdigest(value + self.salt)
	end
	return @password = value
  end

  def moderatorpriv?
  	self.title == 3 or self.title == 5
  end


  def authenticate?(try)
	Digest::SHA256.hexdigest(try + self.salt) == self.password_hash
  end
	
  def self.authenticator(namea="",pass="")
	user = self.find(:first, :conditions => [ "lower(name) = ?", namea.downcase ])
	return user.authenticate?(pass) ? user : nil if user
	return nil
  end
end
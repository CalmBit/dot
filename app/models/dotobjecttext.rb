class Dotobjecttext < ActiveRecord::Base
  attr_accessible :content, :created, :creator, :desc, :dotdowns, :dotups, :nsfw, :private, :title

  acts_as_voteable

  def canView?(user)
    if user
  	  ((self.creator == user.id) && (self.nsfw == 0 || (self.nsfw != 0 && user.age >= 18))) || user.moderatorpriv?
    else
  	  (self.private == 0 && self.nsfw == 0)
    end
  end
  
  belongs_to :user

  has_many :comments
end

class Image < ActiveRecord::Base
  attr_accessible :name, :parent, :sizeX, :sizeY
  belongs_to :user
end

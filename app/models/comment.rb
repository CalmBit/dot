class Comment < ActiveRecord::Base
  attr_accessible :asset, :content, :creator

  acts_as_voteable
  
  belongs_to :dotobjecttext
end

class Dotobjectimage < ActiveRecord::Base
  attr_accessible :creator, :desc, :dotdowns, :dotups, :nsfw, :private, :title

  acts_as_voteable

  belongs_to :user

  has_attached_file :photo
end

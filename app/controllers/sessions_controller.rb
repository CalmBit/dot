class SessionsController < ApplicationController

before_filter :authenticator, only: [:home, :setting, :profile, :feed, :invalid_user, :follow_user, :directory]
before_filter :param_check, only: [:profile]
before_filter :save_login_state, only: [:login, :login_attempt]
#before_filter :check_invalid_id, only: [:invalid_user]

  def home
  	render "home"
  end

  def invalid_user
  end

  def login
		
  end

  def feed
    if !@current_user
      flash[:error] = "You don't have the permissions to do that!"
      redirect_to action: 'home'
    end
  end


  def follow_user
    @user = User.find(params[:id])
    if @current_user and !@current_user.following?(@user)
      @current_user.follow(@user)
      flash[:notice] = "Followed #{@user.name}!"
      redirect_to action: 'profile', id: @user.id
    elsif @current_user.following?(@user)
      flash[:error] = "You're already following #{@user.name}!"
      redirect_to action: 'profile', id: @user.id
    else
      flash[:error] = "You must log in to follow #{@user.name}!"
      redirect_to action: 'profile', id: @user.id
    end
  end

  def directory

  end
  
  def login_attempt
	authorized_user = User.authenticator(params[:username],params[:login_password])
	if authorized_user and !authorized_user.banned
		session[:user_id] = authorized_user.id
		flash[:notice] = "Logged In."
		redirect_to(action: 'home')
	elsif authorized_user and authorized_user.banned
		flash[:error] = "Banned!"
		#flash[:color] = "invalid"
		render "login"
	else
		flash[:error] = "Invalid Username Or Password!"
		#flash[:color] = "invalid"
		render "login"
	end
	end
	
	def logout
		session[:user_id] = nil
		redirect_to action:'home'
	end
  
  def profile
  	begin
  	@dotobjecttexts = Dotobjecttext.where(creator: @user.id)
  end
  end

  def param_check
  	begin
  	@user = User.find(params[:id])
  rescue ActiveRecord::RecordNotFound
  	redirect_to action: 'invalid_user', id: params[:id]
  	return
  	end
  end


  def setting
  end
end
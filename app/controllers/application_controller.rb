class ApplicationController < ActionController::Base
  protect_from_forgery
  
  protected
  
  def authenticator
	unless session[:user_id]
	else
		@current_user = User.find session[:user_id]
		@currid = @current_user.id
		return true
	end
	end
	
  def create_redirect
  	redirect_to(controller: 'sessions',action: 'home') and return
  end

	 def save_login_state
  
	if session[:user_id]
			redirect_to(controller: 'sessions', action: 'home')
			return false
	else
		return true
	end
	end
	
	end
class CommentsController < ApplicationController
	before_filter :authenticator, only: [:create]

	#Quick little create function
	def create
		#Create the comment with the params passed
		@comment = Comment.new(params[:comment])
		#Set the creator
		@comment.creator = @currid
		#Set the ID of the linked dot
		@id = params[:comment][:asset]
		#Saaaaaaaave it
		if @comment.save
			flash[:notice] = "Comment posted!"
			redirect_to "/dots/text?id=#{@id}"
		else
			flash[:error] = "Comment couldn't be posted!"
			redirect_to "/dots/text?id=#{@id}"
		end
	end
end

class DotobjecttextsController < ApplicationController

  before_filter :authenticator, only: [:new, :create, :index, :show, :edit, :votefor, :voteagainst]
  # GET /dotobjecttexts
  # GET /dotobjecttexts.json
  def index
    if @current_user == nil
      flash[:error] = "You aren't logged in!"
      redirect_to(controller: "sessions", action:"home")
    else
      @dotobjecttexts = Dotobjecttext.where(creator: @current_user.id)

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @dotobjecttexts }
      end
    end
  end

  # GET /dotobjecttexts/1
  # GET /dotobjecttexts/1.json
  def show
    @dotobjecttext = Dotobjecttext.find(params[:id])
    @comments = Comment.where(:asset => @dotobjecttext.id).reverse
    @newcomment = Comment.new
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dotobjecttext }
    end
  end

  # GET /dotobjecttexts/new
  # GET /dotobjecttexts/new.json
  def new
    if @current_user
      @dotobjecttext = Dotobjecttext.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @dotobjecttext }
      end
    else 
      flash[:error] = "You don't have the permissions to do that!"
        redirect_to(controller: "sessions", action:"home")
    end
  end

  # GET /dotobjecttexts/1/edit
  def edit
    @dotobjecttext = Dotobjecttext.find(params[:id])
  end

  # POST /dotobjecttexts
  # POST /dotobjecttexts.json
  def create
    @dotobjecttext = Dotobjecttext.new(params[:dotobjecttext])
    @dotobjecttext.creator = @current_user.id
    @dotobjecttext.created = Time.now
    @dotobjecttext.dotups = 0
    @dotobjecttext.dotdowns = 0
    if @dotobjecttext.nsfw == nil
      @dotobjecttext.nsfw = 0
    end
    if @dotobjecttext.save
      flash[:notice] = "Dot posted!"
      redirect_to controller: "sessions", action:"profile", id: @current_user.id
      else
        flash[:error] = "Error with posting!"
        redirect_to controller: "sessions", action:"profile", id: @current_user.id
    end
  end

  # PUT /dotobjecttexts/1
  # PUT /dotobjecttexts/1.json
  def update
    @dotobjecttext = Dotobjecttext.find(params[:id])

    respond_to do |format|
      if @dotobjecttext.update_attributes(params[:dotobjecttext])
        redirect_to controller:"sessions", action:"profile", id: @current_user.id
      else
        format.html { render action: "edit" }
      end
    end
  end

  #ajaxify this entire thing
  def votefor
    @dotobjecttext = Dotobjecttext.find(params[:id])
    if @dotobjecttext.creator == @current_user.id
      flash[:error] = "You can't UpDot your own post!"
      redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
    else
      if @current_user.voted_against?(@dotobjecttext) == false and @current_user.voted_for?(@dotobjecttext) == false
        @current_user.vote_for(@dotobjecttext)
        flash[:notice] = "UpDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
      elsif @current_user.voted_against?(@dotobjecttext)
        @current_user.unvote_for(@dotobjecttext)
        @current_user.vote_for(@dotobjecttext)
        flash[:notice] = "UpDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
      else
        flash[:error] = "You already UpDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
      end
    end
  end

  def voteagainst
    @dotobjecttext = Dotobjecttext.find(params[:id])
    if @dotobjecttext.creator == @current_user.id
      flash[:error] = "You can't DownDot your own post!"
      redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
    else
      if @current_user.voted_against?(@dotobjecttext) == false and @current_user.voted_for?(@dotobjecttext) == false
        @current_user.vote_against(@dotobjecttext)
        flash[:notice] = "DownDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
      elsif @current_user.voted_for?(@dotobjecttext)
        @current_user.unvote_for(@dotobjecttext)
        @current_user.vote_against(@dotobjecttext)
        flash[:notice] = "DownDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
      else
        flash[:error] = "You already DownDotted that post!"
        redirect_to controller:"sessions", action:"profile", id: @dotobjecttext.creator
    end
  end
  end
 #AAAAAAAJAX!

  # DELETE /dotobjecttexts/1
  # DELETE /dotobjecttexts/1.json
  def destroy
    @dotobjecttext = Dotobjecttext.find(params[:id])
    @dotobjecttext.destroy

    respond_to do |format|
      format.html { redirect_to dotobjecttexts_url }
      format.json { head :no_content }
    end
  end
end

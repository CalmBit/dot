class UsersController < ApplicationController
  # GET /users
  # GET /users.json
 # before filter :check_for_current_user, only: [:new]
 #Let it be known that on 15-05-13, a missing underscore on this line caused aprox. 8 hours of grief.
  before_filter :authenticator, only: [:index, :new, :edit, :show, :ban_user, :unban_user]
  before_filter :save_login_state, only:[:new]
  


  
  def index
    if @current_user
      if !@current_user.moderatorpriv?
        flash[:error] = "You don't have the permissions to do that!"
        redirect_to(controller: "sessions", action:"home")
      end
      @users = User.all.sort_by{|e| e[:id]}

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  else
    flash[:error] = "You don't have the permissions to do that!"
        redirect_to(controller: "sessions", action:"home")
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    if @current_user
      redirect_to(action: 'home') and return
    end

    @user = User.new

    respond_to do |format|
      format.html
    end
  end



  def ban_user
    @user = User.find(params[:id])
    if @current_user and @user.banned == false
      if (@current_user.moderatorpriv?) and @user.title != 5
        @user.banned = true
        @user.reason = "Innapropriate content."
        @user.banner = @current_user.name
        @name = @user.name
        @user.save
        flash[:notice] = "User Banned!"
        redirect_to action: 'index'
      elsif (@current_user.moderatorpriv?) and @user.title == 5
        flash[:error] = "User is an admin! Cannot ban!"
        redirect_to action: 'index'
      end
      else
        flash[:error] = "You don't have those permissions!"
        redirect_to action: 'index'
    end
  end

  def unban_user
    @user = User.find(params[:id])
    if @current_user and @user.banned = true
      if @current_user.moderatorpriv?
        @user.banned = false
        @user.reason = ""
        @user.banner = ""
        @user.save
        flash[:notice] = "User Unbanned!"
        redirect_to action: 'index'
      end
      else
        flash[:error] = "You don't have those permissions!"
        redirect_to action: 'index'
    end
  end

 

  # GET /users/1/edit
  def edit
    if @current_user
      @user = User.find params[:user]
      if @current_user.title != 5 and @current_user != @user
        redirect_to(action: 'home')
      end
    else 
      redirect_to(action: 'home')
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    @user.joined = Time.now
    if User.find(:first, :conditions => [ "lower(name) = ?", @user.name.downcase ]) == nil
      if @user.save
        flash[:notice] = "Account Created! Log in to get started!"
        redirect_to controller:'sessions', action:"home"
      else
        flash[:error] = "Error ecountered while creating account!"
      end
    else
      flash[:error] = "A user with that name already exists!"
      redirect_to controller:'sessions', action:"home"
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = "Account Updated! Log in to get started!"
        redirect_to(controller:'sessions', action:"home")
      else
        flash[:error] = "Error encountered while updating account!"
        redirect_to(controller:'sessions', action:"home")
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
end

